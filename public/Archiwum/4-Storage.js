import {
    initializeApp
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js";

import {
    getStorage,
    ref,
    uploadBytesResumable,
    getDownloadURL,
    listAll,
    deleteObject 
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-storage.js";

const firebaseConfig = {
    apiKey: "AIzaSyCzcCu2fNexrZ_jSXyYIFilZukqCMdmTPk",
    authDomain: "arpfrontpl1.firebaseapp.com",
    projectId: "arpfrontpl1",
    storageBucket: "arpfrontpl1.appspot.com",
    messagingSenderId: "967237604679",
    appId: "1:967237604679:web:d53371509730b9ff166862",
    measurementId: "G-8810P5RVR1"
};

// 1. Inicjalizowanie modułów
const app = initializeApp(firebaseConfig);
const storage = getStorage();

// 2. Mapowanie elementów z UI
const imageNameInput = document.querySelector("#imageNameInput");
const selectImageInput = document.querySelector("#selectImageInput");
const uploadtImageBtn = document.querySelector("#uploadtImageBtn");
const uploadProgressLabel = document.querySelector("#uploadProgressLabel");
const uploadedImage = document.querySelector("#uploadedImage");


// ----- WRZUCANIE LOKALNYCH PLIKÓW Z DYSKU DO FIREBASE STORAGE -----
// 3. Proces wybierania pliku z dysku oraz wrzucania go do Storage'a
let imageName = "";
let file = undefined;

selectImageInput.onchange = event => {
    file = event.target.files[0];
    imageName = file.name;
    
    // Jak zabezpieczyć przed takim przypadkiem? a.b.c.jpg -> regex (wyrażenie regularne)
}

uploadtImageBtn.onclick = async() => {
    const metaData = {
        contentType: file.type
    };

    // Stworzenie wskaźnika na adres nowego obrazka
    const storageRef = ref(storage, `images/${imageName}`);

    // Rozpoczęcie procesu wrzucania pliku do Firebase Storage
    // https://firebase.google.com/docs/reference/node/firebase.storage.UploadTask
    const uploadTask = uploadBytesResumable(storageRef, file, metaData);

    uploadTask.on('state-changed', (snapshot) => {
        const progress = `${snapshot.bytesTransferred / snapshot.totalBytes * 100}%`;
        uploadProgressLabel.innerText = progress;

        // if ((snapshot.bytesTransferred / snapshot.totalBytes * 100) > 50 ) uploadTask.cancel();
    },
    (error) => {
        console.warn(error);
    },
    () => {
        getDownloadURL(uploadTask.snapshot.ref)
            .then((url) => {
                uploadedImage.setAttribute("src", url);
            })
    });
};

// 4. Pobieranie wszystkich plików z folderu w Storage'u
const storageImagesRef = ref(storage, "images");
const imagesList = [];

listAll(storageImagesRef)
    .then((listOfImages) => {
        listOfImages.items.forEach((image) => {
            console.log(image._location.path.substring(7));
            imagesList.push(image._location.path);
        })
        displayImages();
    })
    .catch((error) => {
        console.warn("NASZ ERROR: ");
        console.warn(error);
    })
console.log(imagesList);

// 5. Pobieranie URL konkretnego pliku
const displayImages = () => {
    imagesList.forEach((imagePath) => {
        getDownloadURL(ref(storage, imagePath))
            .then((url) => {
                const imgElement = document.createElement("img")
                imgElement.src = url;
                imgElement.width = 50;
                imgElement.height = 50;
                document.body.appendChild(imgElement);
            })
            .catch((error) => {
                console.warn("NASZ ERROR: ");
                console.warn(error);
            })
    });
}

// 6. Usuwanie konkretnego pliku
const imageToDeleteRef = ref(storage, "images/kot.jpg");

/*
const deleteFile = (path) => {

}
deleteObject(imageToDeleteRef)
    .then((res) => {
        console.log(res);
    })


/*

**ZADANIE 2**
1. Pobranie listy wszystkich obrazków (pkt. 4) poprzez funkcję listAll. 
Dzięki temu uzyskamy dostęp do ścieżek każdego z plików.
2. Przygotowanie Dropdown listy (<select>) albo przycisków obok obrazków (obecna lekcja pkt. 5 + CRUD pkt. 9)
3. Przygotowanie funkcji, która przyjmie ścieżkę do pliku w Storage'u oraz usunie ten plik (pkt. 6)
4. Podpięcie do wszystkich przycisków tej funkcji usuwającej (CRUD pkt. 9 ostatnie linie)
*/

const imagesRef = ref(storage, "images");

const generateDropdown = (imagesList) => {
    const selectTag = document.createElement("select");
    selectTag.id = "selectImage";
    document.body.appendChild(selectTag);

    imagesList.forEach((image) => {
        const optionImage = document.createElement("option");
        optionImage.value = image;
        optionImage.text = image.substring(7);
        selectTag.appendChild(optionImage);
    });
};

const generateDeleteButton = () => {
    const button = document.createElement("button");
    button.id = "deleteImageBtn";
    document.body.appendChild(button);
    return button;
};

const deleteImageFromStorage = (imagePath) => {
    const imageRef = ref(storage, imagePath);

    deleteObject(imageRef)
        .then(() => {
            console.log(`Poprawnie usunięto plik ${imagePath}`);
        })
        .catch((error) => {
            console.warn(error);
        })
}

listAll(imagesRef)
    .then((result) => {
        const imagesList = [];
        const imagesPaths = result.items;
        imagesPaths.forEach((image) => {
            imagesList.push(image._location.path_);
        })

        generateDropdown(imagesList);
        generateDeleteButton().onclick = () => {
            const selectImage = document.querySelector("#selectImage");
            deleteImageFromStorage(selectImage.value);
        }
    })
    .catch((error) => {
        console.warn(error);
    })


/*
WSTĘP:
	- Dobór odpowiedniej bazy danych do poszczególnych rozwiązań
	- Stworzenie od zera strony będącej sklepem internetowym

SKLEP:
	- Dostęp dla każdego do listy wszystkich produktów w sklepie (produkty powinny mieć obrazki, nazwe i cene)
	- Możliwość dla zalogowanych dodawania produktów do bazy, edycji, usuwania
        - *Możliwość takich akcji tylko dla osób z uprawnieniami administartora
	- Dostęp dla wszystkich do formularza rejestracji (email, hasło, imie, nazwisko, państwo, stanKonta)
	- Dostęp dla zalogowanych do koszyka
	- Dostęp dla zalogowanych do złożenia zamówienia (przypilnowanie, czy kwota jest wystarczająca)
	- Dostęp dla zalogowanych do panelu klienta (historia zamówień, stan konta, wpłata środków, wypłata środków)
	- Dodanie walidacji (np. czy kwota na koncie wystarczająca do zakupu, sprawdzanie czy wprowadzona kwota nie jest ujemna)
    - Przy wyświetlaniu produktów:
        - Możliwość sortowania produktów po cenie, nazwie *i jednocześnie po cenie i nazwie (indeksy)*
        - *Możliwość wybrania ile wyników maksymalnie ma się pojawić
        -  Możliwość filtrowania wyników do jakiejś ceny

OBSŁUGA KLIENTA:
	- Dostęp dla zalogowanych użytkowników do chatu
	- Przed wiadomością pojawia się nazwa użytkownika/avatar 
	- Możliwość dynamicznego wysyłania wiadomości i dynamicznego odbioru tych wiadomości przez pozostałych użytkowników
*/